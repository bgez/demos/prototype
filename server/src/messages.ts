import { Position } from "./types";

export type MessageType = 'welcome' | 'update' | 'keep-alive' | 'position'

function Is<T extends Message>(type: MessageType): (message: Message) => message is T {
    return (message): message is T => message.type === type
}

export interface Message {
    type: MessageType
    body: any
}

export const WelcomeMessage = Is<WelcomeMessage>('welcome')
export interface WelcomeMessage extends Message {
    type: 'welcome'
    body: {
        name: string
        position: Position
    }
}

export const UpdateMessage = Is<UpdateMessage>('update')
export interface UpdateMessage extends Message {
    type: 'update'
    body: {
        position: [number, number, number]
    }
}

export const KeepAliveMessage = Is<KeepAliveMessage>('keep-alive')
export interface KeepAliveMessage extends Message {
    type: 'keep-alive'
    body: undefined
}

export const PositionMessage = Is<PositionMessage>('position')
export interface PositionMessage extends Message {
    type: 'position',
    body: {
        positions: { [name: string]: Position }
    }
}
