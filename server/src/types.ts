import { Player } from './player';

export type Position = [number, number, number]

export interface Remote {
    address: string
    port: number
}

export interface Client<T> extends Remote {

    /**
     * The referred handle representing the client
     */
    client: T
}

export namespace Client {

    /**
     * Returns some ID for a client/remote
     */
    export function Id(remote: Remote) {
        return `${remote.address}:${remote.port}`
    }

}
