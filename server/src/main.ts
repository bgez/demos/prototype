import { GameServer } from './server';

const server = new GameServer('0.0.0.0', 1234)

server.on('new-client', client =>
    console.log(`New client: ${client.address}:${client.port}`))

server.on('lost-client', client =>
    console.log(`Client timed-out: ${client.address}:${client.port}`))

server.on('ready', () =>
    console.log('Server is ready.'))

server.start()
