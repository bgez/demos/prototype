type PotatoWatcher = () => void

export class Potato<T> {

    protected _callbacks: PotatoWatcher[] = []
    protected _timeout?: NodeJS.Timer

    public cold: boolean = false;

    constructor(
        public readonly value: T,
        public readonly delay: number,
    ) {
        this.heat()
    }

    public heat(): void {
        if (this.cold) return
        if (this._timeout) clearTimeout(this._timeout)
        this._timeout = setTimeout(() => this.dispose(), this.delay)
    }

    public watch(watcher: PotatoWatcher): void {
        if (this.cold) {
            watcher()
            return
        }
        this._callbacks.push(watcher)
    }

    public dispose(): void {
        if (this.cold) return
        this.cold = true;
        this.notify()
    }

    protected notify(): void {
        for (const callback of this._callbacks) {
            try {
                callback()
            } catch (error) {
                console.error(error)
            }
        }
    }
}
